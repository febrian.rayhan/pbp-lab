//Febrian Rayhan Aryadianto
import 'package:flutter/material.dart';
void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = "HelPINK U";
    final ButtonStyle style =
    TextButton.styleFrom(primary: Theme.of(context).colorScheme.onPrimary);

    return MaterialApp(
      title: appTitle,
      theme: new ThemeData(scaffoldBackgroundColor: const Color(0xFFF3F4ED)),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFFD69CA8),
          title: const Text(appTitle),
          actions: <Widget>[

            TextButton(
              style: style,
              onPressed: () {},
              child: const Text('Home'),
            ),

            TextButton(
              style: style,
              onPressed: () {},
              child: const Text('Permohonan'),
            ),

            TextButton(
              style: style,
              onPressed: () {},
              child: const Text('Kritik'),
            ),

            TextButton(
              style: style,
              onPressed: () {},
              child: const Text('Kritik dan Saran'),
            ),

            TextButton(
              style: style,
              onPressed: () {},
              child: const Text('Feedback'),
            )

          ],
        ),
        body: const MyCustomForm(),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(100.0
        ),
        child: Container(
          height: 3000,
          width: 2000,
          padding: EdgeInsets.all(100.0
          ),

          color: Color.fromRGBO(214, 156, 168, 100),
          child: Center(
            child: ListView(
              children: [
                Form(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[

                      Padding(
                        padding: EdgeInsets.only(
                          top: 20.0,
                        ),
                        child: Text("Data Pengaju",
                          style: TextStyle(fontSize: 32,
                            color: Colors.white,
                          ),
                        ),
                      ),

                      Divider(
                        color: Colors.white,
                        indent: 40,
                        endIndent: 40,
                      ),


                      Padding(
                        padding: EdgeInsets.only(
                          top: 20.0,
                        ),
                        child: TextFormField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              fillColor: Colors.white,
                              labelText: "Nama"
                          ),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(
                          top: 20.0,
                        ),
                        child: TextFormField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Tipe Pengajuan"),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(
                          top: 20.0,
                        ),
                        child: TextFormField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Latar"),
                        ),
                      ),


                      Padding(
                        padding: EdgeInsets.only(
                          top: 20.0,
                        ),
                        child: TextFormField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Lokasi"),
                        ),
                      ),


                      Padding(
                        padding: EdgeInsets.only(
                          top: 20.0,
                        ),
                        child: TextFormField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Status"),
                        ),
                      ),


                      Padding(
                        padding: EdgeInsets.only(
                          top: 20.0,
                        ),
                        child: OutlinedButton(
                          style: OutlinedButton.styleFrom(
                            primary: Colors.amberAccent,
                            backgroundColor: Colors.white,
                            minimumSize: Size(150, 50),
                            side: BorderSide(color: Colors.amberAccent),
                          ),
                          onPressed: () {},
                          child: Text("Submit"),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}