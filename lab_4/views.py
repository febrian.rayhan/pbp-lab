from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from lab_2.models import Note
from lab_4.forms import NoteForm

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context = {}
    form = NoteForm()
    if request.method == 'POST':
        form = NoteForm(request.POST)
        form.save()
        return HttpResponseRedirect('/lab-4/')

    context['form'] = form
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)

# Create your views here.
