from django.urls import path
from .views import index, add_note, note_list


urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_note, name='add-note'),
    path('note_list', note_list, name='note_list'),
]
