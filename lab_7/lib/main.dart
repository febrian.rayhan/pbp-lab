import 'package:flutter/material.dart';
import 'package:lab_7/menu/menu_data.dart';
import 'package:lab_7/menu/menu_item.dart';

void main() {
  runApp(const MyApp());
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class MyApp extends StatelessWidget { //Navbar
  const MyApp({Key? key}) : super(key: key);
  @override


  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Color(0xFFC37B89)),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("HelPINK U"),
          backgroundColor: Color(0xFFC37B89),
          actions: [
            PopupMenuButton<MenuNavbar>(
              itemBuilder: (context) => [
                ...MenuData.itemku.map(buildItem).toList(),
              ],
            ),
          ],
        ),
        body: const MyHomePage(
        ),
      ),
    );
  }
  PopupMenuItem<MenuNavbar>buildItem(MenuNavbar item) => PopupMenuItem(
    value: item,
    child: Row(
      children: [
        Icon(item.icon, size: 20, color: Colors.pink.shade100),
        const SizedBox(width: 12),
        Text(item.text),
      ],
    ),
  );
}

class _MyHomePageState extends State<MyHomePage> { //Content
  String nama = '';
  String tipe = '';
  String latar = '';
  String lokasi = '';
  String status = '';
  final _homeKey = GlobalKey<FormState>();
  @override


  Widget build(BuildContext context) {
    final shape = RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(16),
    );
    return Scaffold(
      backgroundColor: const Color(0xFFF3F4ED), 
      body: Form(
        key: _homeKey,
        child: ListView(
        padding: EdgeInsets.all(32),
        children: [
          BuildJudul(),
          const SizedBox(height: 24),
          BuildNama(),
          const SizedBox(height: 24),
          BuildTipe(),
          const SizedBox(height: 24),
          BuildLatar(),
          const SizedBox(height: 24),
          BuildLokasi(),
          const SizedBox(height: 24),
          BuildStatus(),
          const SizedBox(height: 24),
          BuildSubmit(),
          ],
        ),
      ),
    );
  }


  Widget BuildNama() => TextFormField(
    decoration: InputDecoration(
      border: OutlineInputBorder(),
      labelText: "Nama",
      prefixIcon: Icon(Icons.account_circle_outlined, color: Color(0xFFC37B89)),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(16),
        borderSide: BorderSide(color: Color(0xFFC37B89) ,width: 1),
      ),
      labelStyle: TextStyle(
          color: Color(0xFFC37B89),
      ),
    ),
    validator: (value) {
      if (value!.isEmpty) {
        return 'Masukkan Nama Anda';
      } else { return null; }
    },
    onChanged: (value) => setState(() => nama = value),
  );


  Widget BuildTipe() => TextFormField(
    decoration: InputDecoration(
      border: OutlineInputBorder(),
      labelText: "Tipe Pengajuan",
      prefixIcon: Icon(Icons.add_chart_outlined, color: Color(0xFFC37B89)),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(16),
        borderSide: BorderSide(color: Color(0xFFC37B89) ,width: 1),
      ),
      labelStyle: TextStyle(
          color: Color(0xFFC37B89),
      ),
    ),
    validator: (value) {
      if (value!.isEmpty) {
        return 'Masukkan Tipe Ajuan';
      } else { return null; }
    },
    onChanged: (value) => setState(() => tipe = value),
  );


  Widget BuildLatar() => TextFormField(
    decoration: InputDecoration(
      border: OutlineInputBorder(),
      labelText: "Latar",
      prefixIcon: Icon(Icons.add_comment_outlined, color: Color(0xFFC37B89)),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(16),
        borderSide: BorderSide(color: Color(0xFFC37B89) ,width: 1),
      ),
      labelStyle: TextStyle(
          color: Color(0xFFC37B89),
      ),
    ),
    validator: (value) {
      if (value!.isEmpty) {
        return 'Sampaikan Keluhan Anda';
      } else { return null; }
    },
    onChanged: (value) => setState(() => latar = value),
  );


  Widget BuildLokasi() => TextFormField(
    decoration: InputDecoration(
      border: OutlineInputBorder(),
      labelText: "Lokasi",
      prefixIcon: Icon(Icons.add_location_alt_outlined, color: Color(0xFFC37B89)),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(16),
        borderSide: BorderSide(color: Color(0xFFC37B89) ,width: 1),
      ),
      labelStyle: TextStyle(
          color: Color(0xFFC37B89),
      ),
    ),
    validator: (value) {
      if (value!.isEmpty) {
        return 'Masukkan Domisili Anda';
      } else { return null; }
    },
    onChanged: (value) => setState(() => lokasi = value),
  );


  Widget BuildStatus() => TextFormField(
    decoration: InputDecoration(
      border: OutlineInputBorder(),
      labelText: "Status",
      prefixIcon: Icon(Icons.account_balance_outlined, color: Color(0xFFC37B89)),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(16),
        borderSide: BorderSide(color: Color(0xFFC37B89) ,width: 1),
      ),
      labelStyle: TextStyle(
          color: Color(0xFFC37B89),
      ),
    ),
    validator: (value) {
      if (value!.isEmpty) {
        return 'Isi Status Sebagai "Pending"!';
      } else { return null; }
    },
    onChanged: (value) => setState(() => status = value),
  );


  Widget BuildSubmit() => OutlinedButton(
    style: OutlinedButton.styleFrom(
      primary: Colors.amberAccent,
      backgroundColor: Colors.white,
      side: BorderSide(width: 2, color: Colors.amberAccent),
    ),
    onPressed: () {
      final isValid = _homeKey.currentState!.validate();
      if (isValid) {
        _homeKey.currentState!.save();
        final message = 'Data $nama Telah Tersimpan';
        final snackBar = SnackBar(
          content: Text(
            message,
            style: TextStyle(fontSize: 20),
          ),
          backgroundColor: Colors.pink.shade100,
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    },
    child: Text('Submit'),
  );

  Widget BuildJudul() => Text("Data Pengaju",
    style: TextStyle(fontSize: 32,
    color: Color(0xFFC37B89),
    ),
  );
}
