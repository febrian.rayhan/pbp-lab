import 'package:flutter/material.dart';

class MenuNavbar {
  final String text;
  final IconData icon;

  const MenuNavbar({
    required this.text,
    required this.icon,
  });
}