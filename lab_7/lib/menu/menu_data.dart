import 'package:flutter/material.dart';
import 'package:lab_7/menu/menu_item.dart';

class MenuData {
  static const List<MenuNavbar> itemku = [
    navbarHome,
    navbarPermohonan,
    navbarKritik,
    navbarSaran,
    navbarFeedback,
  ];

  static const navbarHome = MenuNavbar(
    text: 'Home',
    icon: Icons.home,
  );

  static const navbarPermohonan = MenuNavbar(
    text: 'Permohonan',
    icon: Icons.volunteer_activism,
  );

  static const navbarKritik = MenuNavbar(
    text: 'Kritik',
    icon: Icons.sms_failed,
  );

  static const navbarSaran = MenuNavbar(
    text: 'Kritik dan Saran',
    icon: Icons.thumbs_up_down,
  );

  static const navbarFeedback = MenuNavbar(
    text: 'Feedback',
    icon: Icons.add_moderator,
  );
}