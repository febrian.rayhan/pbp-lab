1. Apakah perbedaan antara JSON dan XML?

JSON adalah sebuah format untuk melakukan penukaran data ringan tanpa terikat pada sebuah bahasa, namun bahasa universal
yang paling mudah dan sering dilihat digunakan merupakan javascript. Disisi lain XML memiliki fungsi untuk menyimpan dan
membawa/transfer data tanpa fungsi menampilkan data tersebut. XML sendiri merupakan sebuah bahasa bahasa markup dengan
ketentuan dan aturannya sendiri.

2. Apakah perbedaan antara HTML dan XML?

Perbedaan utama dari HTML dan XML adalah bahwa pada dasarnya HTML menampilkan data dan struktur dari sebuah halaman page,
dan HTML itu sudah merupakan suatu bahasa yang sudah ditentukan dengan aturannya sendiri.
Sedangkan XML berfungsi untuk menyimpan dan mentransfer data dengan ketentuan bahasa yang dapat mendefinisikan bahasa lain.