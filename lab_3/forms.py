from lab_1.models import Friend
from django.forms.models import ModelForm

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"