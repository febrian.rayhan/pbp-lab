from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from lab_1.models import Friend
from lab_3.forms import FriendForm

@login_required(login_url='/admin/login/')

def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')

def add_friend(request):
    context = {}
    form = FriendForm()
    if request.method == 'POST':
        form = FriendForm(request.POST)
        form.save()
        return HttpResponseRedirect('/lab-3/')

    context['form'] = form
    return render(request, "lab3_form.html", context)

